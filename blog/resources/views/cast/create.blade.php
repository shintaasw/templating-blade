@extends('master')
@section('judul')
    Halaman Tambah Cast
@endsection

@section('isi')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label for="title">Nama Cast</label>
        <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Title">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Umur</label>
        <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Bio</label>
        <input type="text" class="form-control" name="Bio" id="Bio" placeholder="Masukkan Bio">
        @error('Bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection