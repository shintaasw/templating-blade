@extends('master')
@section('judul')
    Halaman Detail Cast {{$cast->id}}
@endsection

@section('isi')
    
<h2> Nama Cast : {{$cast->nama}}</h2>
<h4>{{$cast->umur}}</h4>
<p>{{$cast->bio}}</p>
@endsection